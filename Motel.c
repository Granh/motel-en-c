/*
			NOTAS VERSION 2.1.1
			
			Se agrega la funcion mostrar Gerente.
			Se agrega la funcion modificar Gerente
			Se agrega la funcino mostrar datos Empleado
			Se modifico la estructura Promociones (paso de una lista simple a un arbol de busqueda)
			se modificaron las funciones eliminar,enlazar de Promociones por sus respectivas de Arboles(borrar, agregarAArbol)
			Se agrega la funcion esHoja, buscar. 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct Motel{

	char *direccion;
	char *nombre;
	struct NodoHabitacion *habitaciones;
	struct NodoEmpleado *empleados;
	struct NodoCliente *clientes;
	struct Empleado *gerente;
	struct NodoPromocion *promociones;

};
struct Promociones{

	char *nombre;
	char *contenido;
	int precio;

};


struct NodoPromocion{

	struct NodoPromocion *derecha;
	struct NodoPromocion *izquierda;
	struct Promociones *contenidoPromocion;

};


struct NodoHabitacion{

	struct Habitacion *pieza;
	struct NodoHabitacion *derecha;

};

struct Habitacion{

	int numero;
	char *tipoDeHabitacion;//NOMBRE
	int ocupada;  


};

struct Cliente{

	char *nombre;
	char *id;
	int numero;

};

struct NodoCliente{

	struct NodoCliente *siguiente;
	struct Cliente *huesped; 

};

struct NodoEmpleado{

	struct Empleado *trabajador;
	struct NodoEmpleado *siguiente;

};
struct Empleado{

	char *nombre;
	int edad;
    char rut[13];
    char *cargo;
    long sueldo;
};



// FUNCIONES MOTEL
struct Motel *crearMotel();
// FUnciones MOTEL


//FUNCIONES CLIENTE
struct NodoCliente *crearNodoCliente(struct NodoHabitacion *);
struct Cliente *crearCliente(struct NodoHabitacion *);
void enlazarNodoCliente(struct NodoCliente **, struct NodoCliente *);
void listadoClientes(struct NodoCliente *);
void eliminarCliente(struct NodoCliente **,char *);
void buscarCliente(char *, struct NodoCliente *);
//FUNCIONES CLIENTE

//FUNCIONES EMPLEADO
struct NodoEmpleado *crearNodoEmpleado();
struct Empleado *crearEmpleado();
void enlazarNodoEmpleado(struct NodoEmpleado **, struct NodoEmpleado *);
void listadoEmpleado(struct NodoEmpleado *);
void eliminarEmpleado(struct NodoEmpleado **,char *);
void buscarEmpleado(char *rut, struct NodoEmpleado *lista);
void mostrarDatosGerente(struct Empleado *);
void modificarEmpleado(struct NodoEmpleado *);
void modificarDatosGerente(struct Empleado *);
//FUNCIONES EMPLEADO

//FUNCIONES PROMOCIONES
struct NodoPromocion *crearNodoPromocion();
char* crearDescripcionPromocion();
struct Promociones *crearPromocion();

struct NodoPromocion *buscar(struct NodoPromocion *, struct NodoPromocion **, int);
void insertarEnArbol(struct NodoPromocion **, struct NodoPromocion *);
int esHoja(struct NodoPromocion *);
void borrar(struct NodoPromocion **, int );
void listadoPreOrden(struct NodoPromocion *);
void listadoInOrden(struct NodoPromocion *);
void listadoPostOrden(struct NodoPromocion *);
//FUNCIONES PROMOCIONES

//FUNCIONES HABITACION
void enlazarNodoHabitacion(struct NodoHabitacion **, struct NodoHabitacion *);
struct NodoHabitacion *crearNodoHabitacion();
struct Habitacion *crearHabitacion();
void listadoHabitaciones(struct NodoHabitacion *);
void listadoHabitacionesDisponibles(struct NodoHabitacion *lista);
void eliminarHabitacion(struct NodoHabitacion **lista, int numero);
void ocuparHabitacion(struct NodoHabitacion **lista, int numeroHabitacion);
void desocuparHabitacion(struct NodoHabitacion **, int);
void listadoHabitacionesOcupadas(struct NodoHabitacion *);
//FUNCIONES HABITACION

//FUNCIONES UTILIDAD
void mostrarMensajeBienvenida();
char *validarNombreConNumeros();
char *validarNombre();
char *llenarChar();
//FUNCIONES UTILIDAD


int main(void){

	int precioPromo = 0;
	char *rutEmpleado;
	char *rutCliente;
	int numeroHabitacion;
	struct Motel *miMotel;
	struct NodoPromocion *auxPromocion;
	struct NodoHabitacion *auxHabitacion;
	struct NodoEmpleado *auxEmpleado;
	struct NodoCliente *auxCliente;
	int controlador = 1;
	miMotel = crearMotel();
	while(controlador != 0){
		
		printf("#######################################\n");
		printf("#		    VERSION 2.0.0			  #\n");
		printf("#-------------------------------------#\n");
		printf("#  Copyright 2020 Motel Mannagement   #\n");	
		printf("#-------------------------------------#\n");
		printf("# Autores: Diego Fuenzalida Diaz      #\n");
		printf("#          Francisca Fuenzalida Diaz  #\n");
		printf("#          Franco Guzman Zavala       #\n");
		printf("#          Jose Toro Carcamo          #\n");
		printf("#######################################\n");
		printf("#          Area Habitaciones          #\n");
		printf("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#\n");
		printf("# Seleccione su opcion:               #\n");
		printf("# 1.Agregar una habitacion.			  #\n");
		printf("# 5.Quitar una habitacion.			  #\n");
		printf("# 9.Lista de habitaciones.			  #\n");
		printf("# 10.Lista de habitaciones disponibles#\n");
		printf("#######################################\n");
		printf("#          Area Promociones           #\n");
		printf("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#\n");
		printf("# Seleccione su opcion:				  #\n");
		printf("# 2. Agregar una promocion.           #\n");
		printf("# 6. Quitar una promocion.            #\n");
		printf("# 11. Lista de promociones.			  #\n");
		printf("#######################################\n");
		printf("#            Area Empleados           #\n");
		printf("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#\n");
		printf("# Seleccione su opcion: 			  #\n");
		printf("# 3. Agregar un empleado.			  #\n");
		printf("# 7. Eliminar un empleado.            #\n");
		printf("# 12. Lista de empleados. 			  #\n");
		printf("# 15. Buscar un empleado.			  #\n");
		printf("# 17. Modificar un empleado.          #\n");
		printf("# 16. Ver datos del gerente.          #\n");
		printf("#######################################\n");
		printf("#            Area Clientes            #\n");
		printf("#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#\n");
		printf("# Seleccione su opcion:				  #\n");
		printf("# 4. Agregar un cliente.              #\n");
		printf("# 8. Eliminar un cliente.			  #\n");
		printf("# 13. Lista de clientes. 			  #\n");
		printf("# 14. Modificar un cliente.           #\n");
		printf("# 18. Modificar al gerente.           #\n");
		printf("#######################################\n");
		printf("# 0.Cerrar el programa.				  #\n");
		printf("#######################################\n");
		
		scanf("%i",&controlador);
		if(controlador == 1){
			auxHabitacion = crearNodoHabitacion();
            enlazarNodoHabitacion(&(miMotel->habitaciones),auxHabitacion);
			printf("Habitacion creada.\n");
		}
		else if(controlador == 2){
			auxPromocion = crearNodoPromocion();
			insertarEnArbol(&(miMotel->promociones),auxPromocion); 
			printf("Promocion creada.\n");

		}
		else if(controlador == 3){
			auxEmpleado = crearNodoEmpleado();
			enlazarNodoEmpleado(&(miMotel->empleados),auxEmpleado);
			printf("Empleado creado.\n");
		}
		else if(controlador == 4){
			auxCliente = crearNodoCliente(miMotel->habitaciones);
			enlazarNodoCliente(&(miMotel->clientes),auxCliente);
			numeroHabitacion = auxCliente->huesped->numero;
			ocuparHabitacion(&(miMotel->habitaciones),numeroHabitacion);//HACE QUE LA HABITACION PASE A ESTAR OCUPADA
			printf("Cliente creado.\n");

		}
		else if(controlador == 5){
			printf("Ingrese el numero de la habitacion.\n");
			scanf("%i",&numeroHabitacion);
			eliminarHabitacion(&(miMotel->habitaciones),numeroHabitacion);
			printf("Habitacion eliminada.\n");
		}
		else if(controlador == 6){
			listadoInOrden(miMotel->promociones);
			printf("Ingrese el precio de la promocion a eliminar: ");
			scanf("%i",&precioPromo);
			borrar(&(miMotel->promociones),precioPromo);
			printf("Promocion eliminada.\n");
		}
		else if(controlador == 7){
			printf("Ingrese rut del empleado : ");
			rutEmpleado = llenarChar();
			eliminarEmpleado(&(miMotel->empleados),rutEmpleado);
			printf("Empleado eliminado.\n");
		}
		else if(controlador == 8){
			printf("Ingrese id del cliente : ");
			rutCliente = llenarChar();
			eliminarCliente(&(miMotel->clientes),rutCliente);
			printf("\nIngrese el numero de la habitacion en que se alojaba : ");
			scanf("%i",&numeroHabitacion);
			desocuparHabitacion(&(miMotel->habitaciones),numeroHabitacion);

		
			numeroHabitacion = 0;
			printf("Cliente eliminado.\n");
		}
		else if(controlador == 9){
			listadoHabitaciones(miMotel->habitaciones);
		}
		else if(controlador == 10){
			listadoHabitacionesDisponibles(miMotel->habitaciones);
		}
		else if(controlador == 11){
			listadoInOrden(miMotel->promociones);
		}
		else if(controlador == 12){
			listadoEmpleado(miMotel->empleados);
		}
		else if(controlador == 13){
			listadoClientes(miMotel->clientes);
		}
		else if(controlador  == 14){
			printf("Ingrese rut cliente : ");
			rutCliente = llenarChar();
			buscarCliente(rutCliente, miMotel->clientes);
		}
		else if(controlador == 15){
			printf("Ingrese rut empleado : ");
			rutEmpleado = llenarChar();
			buscarEmpleado(rutEmpleado,miMotel->empleados);
		}
		else if(controlador == 16){
			mostrarDatosGerente(miMotel->gerente);
		}
		else if(controlador == 17){
			modificarEmpleado(miMotel->empleados);
		}
		else if(controlador == 18){
			modificarDatosGerente(miMotel->gerente);
		}
		else if(controlador == 0){
			printf("GRACIAS POR USAR MOTEL MANNAGEMENT!!\n");
		}
		else
			printf("INGRESE UN NUMERO VALIDO.\n");


	}
	return 0;
}



//FUNCIONES------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//CLIENTES------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//CLIENTES------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//CLIENTES------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
struct NodoCliente *crearNodoCliente(struct NodoHabitacion *lista){

	struct NodoCliente *miNodo;
	miNodo = (struct NodoCliente *)malloc(sizeof(struct NodoCliente));
	miNodo->huesped = crearCliente(lista);
	miNodo->siguiente = NULL;
	return miNodo;
}

struct Cliente *crearCliente(struct NodoHabitacion *lista){
 
	

	struct Cliente *miCliente = (struct Cliente *)malloc(sizeof(struct Cliente));
	printf("Ingrese el nombre de su cliente : ");
	miCliente->nombre = validarNombre();
	printf("Ingrese id del cliente : ");
	miCliente->id = llenarChar();
	listadoHabitacionesDisponibles(lista);
	printf("Ingrese el numero de habitacion : ");
	scanf("%i",&miCliente->numero);
	getchar();
	return miCliente;

}

void enlazarNodoCliente(struct NodoCliente **lista, struct NodoCliente *miCliente){

	struct NodoCliente *recorrido;
	recorrido = *lista;

	if(*lista == NULL){
		*lista = miCliente;
	}
	else{
		while(recorrido){
			if(recorrido->siguiente == NULL){
				recorrido->siguiente = miCliente;
                recorrido = recorrido->siguiente;
			}
			recorrido = recorrido->siguiente;
		}
	}
}



void eliminarCliente(struct NodoCliente **lista, char *id){

	struct NodoCliente *rec;
	int numero;
	rec = *lista;
	if(*lista == NULL){
		printf("NO HAY CLIENTES.\n");
		return;

	}
	else if((strcmp((*lista)->huesped->id,id) == 0) && ((*lista)->siguiente == NULL)){//UNICO ELEMENTO DE LA LISTA
		
		printf("\nDATOS CLIENTE ELIMINADO\nNombre : %s\nID : %s\nNumero habitacion : %i\n",(*lista)->huesped->nombre,(*lista)->huesped->id,(*lista)->huesped->numero);
		*lista = NULL;
		printf("\nSe eliminaron todos los clientes.\n");
		return;
	}
	else if((strcmp((*lista)->huesped->id,id) == 0) && ((*lista)->siguiente != NULL)){//PRIMER ELEMENTO SE TIENE QUE ELIMINAR PERO HAY UNO SEGUNDO
		

		printf("\nDATOS CLIENTE ELIMINADO\nNombre : %s\nID : %s\nNumero habitacion : %i\n",(*lista)->huesped->nombre,(*lista)->huesped->id,(*lista)->huesped->numero);
		*lista = (*lista)->siguiente;
		return;
	}
	else{

		while(rec != NULL){
			if((strcmp(rec->siguiente->huesped->id,id) == 0)&& (rec->siguiente->siguiente == NULL)){//ultimo dato de la lista
				printf("\nDATOS CLIENTE ELIMINADO\nNombre : %s\nID : %s\nNumero habitacion : %i",rec->siguiente->huesped->nombre,rec->siguiente->huesped->id,rec->huesped->numero);
				rec->siguiente = NULL;
				return;
				
			}
			if((strcmp(rec->siguiente->huesped->id,id) == 0)&& (rec->siguiente->siguiente != NULL)){//dato intermedio de la lista
				

				printf("\nDATOS CLIENTE ELIMINADO\nNombre : %s\nID : %s\nNumero habitacion : %i\n",rec->siguiente->huesped->nombre,rec->siguiente->huesped->id,rec->siguiente->huesped->numero);
				rec->siguiente = rec->siguiente->siguiente;
				return;
				
			}
			rec = rec->siguiente;

		}
		
	}

}
//CLIENTES------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//CLIENTES------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//CLIENTES------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



struct NodoEmpleado *crearNodoEmpleado(){

	struct NodoEmpleado *miNodo;
	miNodo =(struct NodoEmpleado *)malloc(sizeof(struct NodoEmpleado));
	miNodo->trabajador = crearEmpleado();
	miNodo->siguiente = NULL;// EL SIGUIENTE QUEDA VACIO
	return miNodo;
}



struct NodoHabitacion *crearNodoHabitacion(){

	struct NodoHabitacion *miNodo;
	miNodo =(struct NodoHabitacion *)malloc(sizeof(struct NodoHabitacion));
	miNodo->pieza = crearHabitacion();
	miNodo->derecha = NULL;


	return miNodo;

}


struct NodoPromocion *crearNodoPromocion(){

	struct NodoPromocion *miNodo;
	miNodo = (struct NodoPromocion *)malloc(sizeof(struct NodoPromocion));
	miNodo->contenidoPromocion = crearPromocion();
	miNodo->derecha = NULL;
	miNodo->izquierda = NULL;

	return miNodo;
}


struct Habitacion *crearHabitacion(){

	struct Habitacion *miHabitacion;
	miHabitacion = (struct Habitacion*)malloc(sizeof(struct Habitacion));
	printf("Ingrese el numero de su habitacion : ");
	scanf("%i",&miHabitacion->numero);
	getchar();
    printf("Ingrese nombre habitacion : ");
	miHabitacion->tipoDeHabitacion = validarNombreConNumeros();
	miHabitacion->ocupada = 0; // EL 0 INDICA QUE ESTA DESOCUPADA , 1 PARA OCUPADA

	return miHabitacion;
	
}

void mostrarMensajeBienvenida(){
	printf("Bienvenido a su software de administracion motelera.\nA continuacion ingrese los datos de su motel.\n");
	return;
}

char *llenarChar(){

    char buffer[200];
    char *cadena;
    scanf(" %[^\n]",buffer);
    getchar();
    cadena =(char *)malloc (sizeof(char) * (strlen(buffer) + 1)); 
    strcpy(cadena,buffer);
    return cadena;
}

struct Empleado *crearEmpleado(){
    
    char rut[13];
	struct Empleado *trabajador;
	trabajador = (struct Empleado *)malloc(sizeof(struct Empleado));
	printf("Ingrese un nombre : ");
	trabajador->nombre = validarNombre();
	printf("Ingrese un rut : ");
    scanf("%[^\n]",rut);
    getchar();
    strcpy(trabajador->rut,rut);
	printf("Ingrese un cargo : ");
	trabajador->cargo = validarNombreConNumeros();
	printf("Ingrese una edad : ");
	scanf("%i",&trabajador->edad);
	getchar();
	printf("Ingrese un sueldo estimado : ");
	scanf("%ld",&trabajador->sueldo);
	getchar();
	return trabajador;
}

struct Motel *crearMotel(){ // SE ENCARGA DE LLENAR LOS DATOS BASICOS DE SU MOTEL
	
	struct Motel *miMotel;
	miMotel =(struct Motel *)malloc(sizeof(struct Motel));//MEMMORIA PARA EL MOTEL
	mostrarMensajeBienvenida();
	//LLENADO DE DATOS BASICOS -------------------------------------------------------
	printf("Ingrese el nombre de su motel : ");
	miMotel->nombre = llenarChar();
	printf("Ingrese la direccion : ");
	miMotel->direccion = llenarChar();
	printf("Ingrese los datos de su gerente\n");
	miMotel->gerente = crearEmpleado();
	printf("Por favor cree una habitacion.\n");
	miMotel->habitaciones = crearNodoHabitacion();
	miMotel->empleados = NULL;
	miMotel->promociones = NULL;
	miMotel->clientes = NULL;
	return miMotel;
}

char *validarNombreConNumeros(){

	char *cadena;
	int i, flag,cont;
	flag = 0;



	while(flag == 0){
		cont = 0;
		cadena = llenarChar();
		for(i = 0; i < strlen(cadena); i++){
			if(ispunct(cadena[i]))
				continue;
			cont = cont + 1;
		}
		if(cont == strlen(cadena))
			flag = 1;
        else
            printf("Ingrese solo letras o numeros.");
	}
	return cadena;

} 
 // El nombre de habitacion puede tener numero y letras nada mas.

char *validarNombre(){

	char *cadena;
	int i, flag,cont;
	flag = 0;
	do{
        cont = 0;
		cadena = llenarChar();
		for(i = 0; i < strlen(cadena);i++){
			if(!isalpha(cadena[i]))// si el char no es una letra no cuenta
				continue;
			cont = cont + 1;
		}
		if(cont == strlen(cadena))
			flag = 1;
		else
			printf("Ingrese solo letras : ");
	}while(flag == 0);
    return cadena;

}

 struct Promociones *crearPromocion(){

 	struct Promociones *miPromocion;
 	miPromocion = (struct Promociones *)malloc(sizeof(struct Promociones));
 	printf("Ingrese el nombre de su promocion : ");
 	miPromocion->nombre = validarNombreConNumeros();
 	miPromocion->contenido = crearDescripcionPromocion();
 	printf("Ingrece el precio de la promocion : ");
 	scanf("%i",&miPromocion->precio);
 	return miPromocion;
 }

char *crearDescripcionPromocion(){

 	char buffer[500];
 	char *cadena;
 	printf("Ingrese una breve descripcion de su promocion.\n");
 	scanf("%[^\n]",buffer);
 	getchar();
 	cadena =(char *)malloc((sizeof(char) * strlen(buffer) ) + 1);
 	strcpy(cadena,buffer);
 	return cadena;
 }

 void enlazarNodoHabitacion(struct NodoHabitacion **lista, struct NodoHabitacion *nodo){

	struct NodoHabitacion *recorrido;
	recorrido = *lista;
	if(*lista != NULL){
		while(recorrido != NULL){
			if(recorrido->derecha == NULL){
				recorrido->derecha = nodo;
				recorrido = recorrido->derecha;	
			}
			recorrido = recorrido->derecha;
		}


	}
	else
		*lista = nodo;


}

void enlazarNodoEmpleado(struct NodoEmpleado **lista, struct NodoEmpleado *sujeto){

	struct NodoEmpleado *recorrido;
	recorrido = *lista;
	if(*lista == NULL)
		*lista = sujeto;
	else{
		while(recorrido != NULL){
			if(recorrido->siguiente == NULL){
				recorrido->siguiente = sujeto;
				recorrido = recorrido->siguiente;
			}
			recorrido = recorrido->siguiente;

		}

	}

}




void listadoEmpleado(struct NodoEmpleado *lista){
 
 	struct NodoEmpleado *rec = lista;
 	if(lista == NULL){
 		printf("No hay empleados.\n");
 	}
    while(rec != NULL){
        printf("\n\nNombre : %s\nEdad : %i\nRut : %s\nCargo : %s\nSueldo : %li\n\n",rec->trabajador->nombre,rec->trabajador->edad,rec->trabajador->rut,rec->trabajador->cargo,rec->trabajador->sueldo);
     rec= rec->siguiente;
    }
}

void listadoHabitaciones(struct NodoHabitacion *lista){

	struct NodoHabitacion *rec = lista;
	if(lista == NULL){
		printf("No hay habitaciones.\n");
		return;
	}
	while(rec != NULL){
		printf("\n\nNumero : %i\nNombre : %s\nOcupada : %i\n\n",rec->pieza->numero,rec->pieza->tipoDeHabitacion,rec->pieza->ocupada);
		rec = rec->derecha;
	}


}
void listadoHabitacionesDisponibles(struct NodoHabitacion *lista){


	if(lista == NULL){
		printf("No hay habitaciones.\n");
	}
	while(lista != NULL){
		if(lista->pieza->ocupada == 0){
			printf("\n\nNumero : %i\nNombre : %s\nOcupada : %i\n\n",lista->pieza->numero,lista->pieza->tipoDeHabitacion,lista->pieza->ocupada);
		}
		lista = lista->derecha;
	}
	printf("No hay habitaciones disponibles.\n");
	

}


void listadoClientes(struct NodoCliente *lista){

	struct NodoCliente *rec;
	if(lista == NULL){
		printf("No hay clientes.\n");
		return;
	}
	rec = lista;
	while(rec){
		printf("Nombre : %s\nID : %s\n",rec->huesped->nombre, rec->huesped->id);
		rec = rec->siguiente;
	}
}




void eliminarEmpleado(struct NodoEmpleado **lista,char *rut){
    
    struct NodoEmpleado *rec = *lista;
    if(*lista == NULL){//LISTA VACIA
        printf("NO HAY EMPLEADOS");
        return;
    }
    else if((strcmp((*lista)->trabajador->rut,rut) == 0) && ((*lista)->siguiente != NULL)){//PRIMER ELEMENTO DE LA LISTA 
        *lista = (*lista)->siguiente;
        return;
    }
    else if((strcmp((*lista)->trabajador->rut,rut) == 0) && ((*lista)->siguiente == NULL)){
    	*lista = NULL;
    	printf("Se eliminaron todos los empleados.\n");
    	return;


    }
    else{
        while(rec){
            if(strcmp(rec->siguiente->trabajador->rut,rut) == 0 && (rec->siguiente->siguiente == NULL)){//ES EL ULTIMO ELEMENTO
                rec->siguiente = NULL;
                return;
            }
            
            if(strcmp(rec->siguiente->trabajador->rut,rut)== 0){//ALGUN ELEMENTO ENTREINICIO Y  FINAL
                rec->siguiente = rec->siguiente->siguiente;
                return;
            }
            
            rec = rec->siguiente;
        }
   
        
        
        
    }
    printf("NO SE ENCONTRO AL EMPLEADO\n\n");
}


void eliminarHabitacion(struct NodoHabitacion **lista, int numero){

    struct NodoHabitacion *rec;
    rec = *lista;
    
    if(*lista == NULL){
        printf("NO HAY HABITACIONES QUE ELIMINAR.\n");
    
    }
    else if(((*lista)->pieza->numero == numero) && ((*lista)->derecha == NULL)){
    	printf("Se han borrado todas las habitaciones.\n");
    	*lista = NULL;

    }
    else if(((*lista)->pieza->numero == numero) && ((*lista)->derecha != NULL)){
    	*lista = (*lista)->derecha;


    }
    else{
    	while(rec){
    		if((rec->derecha->pieza->numero == numero) && (rec->derecha->derecha == NULL)){
    			rec->derecha = NULL;
    			return;
    		}
    		if((rec->derecha->pieza->numero == numero) && (rec->derecha->derecha != NULL)){
    			rec->derecha = rec->derecha->derecha;
    			return;
    		}
    	
    		rec = rec->derecha;
    	}
    }
    
}
void ocuparHabitacion(struct NodoHabitacion **lista, int numeroHabitacion){


	struct NodoHabitacion *rec = *lista;
	while(rec){
		if(rec->pieza->numero == numeroHabitacion){
			rec->pieza->ocupada = 1;
		}
		rec = rec->derecha;
		
			
	}
}
void buscarCliente(char *id, struct NodoCliente *lista){
	struct NodoCliente *rec;
	if (lista == NULL){
		printf("No hay clientes.\n");
		return;
	}
	rec = lista;
	while(rec){
		if(strcmp(id,lista->huesped->id) == 0)
			printf("El cliente si se encuentra registrado.\n");
		rec = rec->siguiente;
	}
	printf("EL cliente no se encuentra registrado.\n");
}
void buscarEmpleado(char *rut, struct NodoEmpleado *lista){
	if(lista == NULL){
		printf("No hay empleados.\n");
		return;
	}
	struct NodoEmpleado *rec;
	rec = lista;
	while(rec){
		if(strcmp(rec->trabajador->rut,rut) == 0)
			printf("El empleado si existe.\n");
		rec = rec->siguiente;
	}
	printf("El empleado no existe.\n");
}
void mostrarDatosGerente(struct Empleado *trabajador){
	printf("\n\nNombre : %s\nEdad : %i\nRut : %s\nCargo : %s\nSueldo : %li\n\n",trabajador->nombre,trabajador->edad,trabajador->rut,trabajador->cargo,trabajador->sueldo);
	
}

void desocuparHabitacion(struct NodoHabitacion **lista, int numeroHabitacion){

	
	struct NodoHabitacion *rec = *lista;
	while(rec){
		if(rec->pieza->numero == numeroHabitacion){
			rec->pieza->ocupada = 0;
		}
		rec = rec->derecha;		
	}
	return;
	
}
void listadoHabitacionesOcupadas(struct NodoHabitacion *lista){
	struct NodoHabitacion *rec = lista;
	
	while(rec){
		
		if(rec->pieza->numero == 1){
			printf("\n\nNumero : %i\nNombre : %s\nOcupada : %i\n\n",rec->pieza->numero,rec->pieza->tipoDeHabitacion,rec->pieza->ocupada);
		}
		rec = rec->derecha;
		
		
	}
}
void modificarEmpleado(struct NodoEmpleado *lista){
	
	char *rut;
	long sueldo;
	struct NodoEmpleado *rec = lista;
	listadoEmpleado(lista);
	printf("Ingrese el rut del empleado a modificar : ");
	rut = llenarChar();
	while(rec){
		if(strcmp(rec->trabajador->rut, rut)== 0){
			printf("Ingrese su nuevo nombre : ");
			rec->trabajador->nombre = validarNombre();
			printf("Ingrese su nuevo rut : ");
			rut = llenarChar();
			strcpy(rec->trabajador->rut,rut);
			printf("Ingrese su nuevo sueldo : ");
			scanf("%i",&sueldo);
			getchar();
			rec->trabajador->sueldo = sueldo;
			printf("Ingrese nuevo cargo : ");
			rec->trabajador->cargo = validarNombreConNumeros();
		}
		rec = rec->siguiente;
	}
	
	
}
struct NodoPromocion *buscar(struct NodoPromocion *arbol, struct NodoPromocion **ant, int precio){


	int enc = 0;
	while(!enc && (arbol)){
		if(arbol->contenidoPromocion->precio == precio){
			enc = 1;//SE ENCONTRO POR LO QUE SE SALE DEL WHILE
		}
		else{
			*ant = arbol;
			if(precio < arbol->contenidoPromocion->precio)
				arbol = arbol->izquierda;
			else
				arbol = arbol->derecha;
		}
	}
	return arbol;//RETORNA NULL SI NO LO ENCUENTRA, Y SI LO ENCUENTRA RETORNA EL LUGAR EN EL QUE SE ENCUENTRA 
}
void insertarEnArbol(struct NodoPromocion **arbol, struct NodoPromocion *nodo){

	struct NodoPromocion *nuevo = NULL;
	struct NodoPromocion *anterior = NULL;
	struct NodoPromocion *aux = NULL;

	aux = buscar(*arbol,&anterior,nodo->contenidoPromocion->precio);
	if(!aux){//NO SE ENCONTRO
		if(!anterior){
			*arbol = nodo;
		}
		else{
			if(nodo->contenidoPromocion->precio < anterior->contenidoPromocion->precio){
				anterior->izquierda = nodo;
			}
			else
				anterior->derecha = nuevo;
		}
	}
	else
		printf("\nPRECIO YA INSCRITO POR FAVOR USAR OTRO\n");

}

int esHoja(struct NodoPromocion *nodo){
	if((!nodo->derecha) && (!nodo->izquierda)) // NO TIENE HIJOS
		return 1;
	return 0;//SI TIENE HIJOS
}
void borrar(struct NodoPromocion **arbol, int precio){

	struct NodoPromocion *padre = NULL;
	struct NodoPromocion *actual = NULL;
	struct NodoPromocion *nodo = NULL;
	int aux;
	actual = buscar(*arbol,&padre,precio);

	while(actual){
		if(esHoja(actual)){
			if(padre){
				if(padre->derecha == actual){
					padre->derecha = NULL;
				}
				else
					padre->izquierda = NULL;

			}
			free(actual);
			actual = NULL;
			return;
		}
		else{
			padre = actual;
			if(actual->derecha){
				nodo = actual->derecha;
				while(nodo->izquierda){
					padre = nodo;
					nodo = nodo->izquierda;
				}

			}
			else{
				nodo = actual->izquierda;
				while(nodo->derecha){
					padre = nodo;
					nodo = nodo->derecha;
				}
			}
			aux = actual->contenidoPromocion->precio;
			actual->contenidoPromocion->precio = nodo->contenidoPromocion->precio;
			nodo->contenidoPromocion->precio = aux;
			actual = nodo;
		}
	}
}
void listadoPreOrden(struct NodoPromocion *arbol){
	if(arbol){
		printf("\nNombre : %s\nPrecio : %i\nDescripcion : %s\n",arbol->contenidoPromocion->nombre,arbol->contenidoPromocion->precio,arbol->contenidoPromocion->contenido);
		listadoPreOrden(arbol->izquierda);
		listadoPreOrden(arbol->derecha);
	}
	return;
}

void listadoInOrden(struct NodoPromocion *arbol){
	
	if(arbol){
		listadoInOrden(arbol->izquierda);
		printf("\nNombre : %s\nPrecio : %i\nDescripcion : %s\n",arbol->contenidoPromocion->nombre,arbol->contenidoPromocion->precio,arbol->contenidoPromocion->contenido);
		listadoInOrden(arbol->derecha);
	}
	return;
}
void listadoPostOrden(struct NodoPromocion *arbol){
	if(arbol){
		listadoPostOrden(arbol->izquierda);
		listadoPostOrden(arbol->derecha);
		printf("\nNombre : %s\nPrecio : %i\nDescripcion : %s\n",arbol->contenidoPromocion->nombre,arbol->contenidoPromocion->precio,arbol->contenidoPromocion->contenido);
	}
	return;
}
void modificarDatosGerente(struct Empleado *gerente){
	mostrarDatosGerente(gerente);
	printf("\n\n");
	gerente = crearEmpleado();

}
